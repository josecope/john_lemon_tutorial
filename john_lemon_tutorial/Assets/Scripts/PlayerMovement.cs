﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
//directives, allow you to use code implemented throughout the script

public class PlayerMovement : MonoBehaviour
{ //class declaration, public means it can be edited in unity

    public float turnSpeed = 20f;
    //creates a new public float variable called turnSpeed and adds a speed

    Animator m_Animator;
    //references the animator component

    Rigidbody m_Rigidbody;
    //sets the rigidbody reference

    AudioSource m_AudioSource;
    //sets the audio reference for footsteps

    Vector3 m_Movement;
    //creates a vector3 variable to be used

    Quaternion m_Rotation = Quaternion.identity;

    public GameObject enemyToSave; //references a ghost you can save
    //stores rotation information

    public GameObject FX;

    public ParticleSystem spiritParticles;

    // Start is called before the first frame update
    void Start()
    {
        m_Animator = GetComponent<Animator>();
        //fully references the animator

        m_Rigidbody = GetComponent<Rigidbody>();
        //references the rigidbody

        m_AudioSource = GetComponent<AudioSource>();
        //references the audio
    }

    // Update is called once per frame
    void FixedUpdate()
    {
        float horizontal = Input.GetAxis("Horizontal");
        //creates new float variable equal to the result of this call
        float vertical = Input.GetAxis("Vertical");
        //creates another new float variable for the vertical axis instead

        m_Movement.Set(horizontal, 0f, vertical);
        m_Movement.Normalize();
        //creates movement and normalizes the magnitude of horizontal movement

        bool hasHorizontalInput = !Mathf.Approximately(horizontal, 0f);
        //creates an inverted(!) bool variable to tell whether John's horizontal is 0
        bool hasVerticalInput = !Mathf.Approximately(vertical, 0f);
        //does the same thing for the vertical axis
        bool isWalking = hasHorizontalInput || hasVerticalInput;
        //combines these into a single bool variable, both must equate to true or else it is false
        m_Animator.SetBool("IsWalking", isWalking);
        //sets the value of the animator parameter
        if (isWalking)
        {
            if (!m_AudioSource.isPlaying)
            {
                m_AudioSource.Play(); //if john is walking and the audio source is NOT(!) playing, play the audio
            }
        }
        else
        {
            m_AudioSource.Stop(); //stop the audio
        }

        Vector3 desiredForward = Vector3.RotateTowards(transform.forward, m_Movement, turnSpeed * Time.deltaTime, 0f);
        //creates a new vector3 variable for setting rotation
        m_Rotation = Quaternion.LookRotation(desiredForward);
        //creates a rotation in the direction of the parameter
    }
    void OnAnimatorMove() //allows us to apply root motion however we want, meanining movement and rotation can be applied to John separately
    {
        m_Rigidbody.MovePosition(m_Rigidbody.position + m_Movement * m_Animator.deltaPosition.magnitude);
        //taking magnitude and multiplying by the movement vector to create movement

        m_Rigidbody.MoveRotation(m_Rotation);
        //directly sets the rotation
    }

    private void Update()
    {
        if (Input.GetKeyDown(KeyCode.Space) && enemyToSave != null)
        { //destroyes the saved enemy with the press of the f button
            Destroy(enemyToSave); //destroy the object stored in the variable
            Time.timeScale = 1f; //set the passing of time back to the standard
            spiritParticles.Play(); //create a particle effect when a ghost is saved

        }
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.CompareTag("SaveZone")) //checks to see if the ghost is in range of the Saving Zone
        {
            enemyToSave = other.gameObject.transform.parent.gameObject; //set the parent object to be the thing that is destroyed
            Time.timeScale = .5f;
        }
    }

    private void OnTriggerExit(Collider other)
    {
        if (other.gameObject.CompareTag("SaveZone"))
        {
            if (other.gameObject.transform.parent.gameObject == enemyToSave)
            {
                enemyToSave = null; //clears the enemyToSave variable since the ghost has left the saving zone
                Time.timeScale = 1f;
            }
        }
    }
}
