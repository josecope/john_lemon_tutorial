﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class GameEnding : MonoBehaviour
{
    public float fadeDuration = 1f;

    public float displayImageDuration = 1f;

    public GameObject player;

    public CanvasGroup exitBackgroundImageCanvasGroup;
    public AudioSource exitAudio;
    //references these as public variables

    public CanvasGroup caughtBackgroundImageCanvasGroup;
    public AudioSource caughtAudio;


    bool m_IsPlayerAtExit; //asks whether the player is at the game exit
    bool m_IsPlayerCaught; //asks whether the player is caught
    float m_Timer;
    bool m_HasAudioPlayed; //asks whether the audio has already been played

    public GameObject textDisplay; //create a variable for the countdown to be put in
    public int secondsLeft = 50;  //seconds on the clock
    public bool takingAway = false;

    private void Start()
    {
        textDisplay.GetComponent<Text>().text = "00:" + secondsLeft;  //displays the time left at the start
    }




    void OnTriggerEnter(Collider other)
    {
        if (other.gameObject == player)
        {
            m_IsPlayerAtExit = true;
            //determines if the player is at the exit location

        }
    }



    public void CaughtPlayer()
    {
        m_IsPlayerCaught = true;
        //determines if the player has been caught
    }

    IEnumerator TimerTake() //timer function
    {
        takingAway = true;  //taking away time is true
        yield return new WaitForSeconds(1); //wait for a second before subtracting any time
        secondsLeft -= 1;  //tells the loop how much time to subtract each second
        textDisplay.GetComponent<Text>().text = "00:" + secondsLeft; //displays the timer
        takingAway = false; //ends the timer function
    }

    void Update()
    {
        if (takingAway == false && secondsLeft > 0) //begins the routine if taking away is false and if seconds left is greater than 0
        {
            StartCoroutine(TimerTake()); //starts the routine of TimerTake that is defined above
        }


        if (m_IsPlayerAtExit)
        {
            EndLevel(exitBackgroundImageCanvasGroup, false, exitAudio); //allows the game to activate the exit audio and win screen
        }
        else if (m_IsPlayerCaught)
        {
            EndLevel(caughtBackgroundImageCanvasGroup, true, caughtAudio); //allows the game to do the same as above except with the caught audio and screen
        }
       
    } //these conditions allow the level to be ended and will bring up the ending screens

    void EndLevel(CanvasGroup imageCanvasGroup, bool doRestart, AudioSource audioSource)
    {
        if (!m_HasAudioPlayed)
        {
            audioSource.Play(); //this if statement allows us to play the audio if it has not been played yet (take note of the !)
            m_HasAudioPlayed = true; //sets the has played variable to true, making it so that the audio will not play again
        }
        m_Timer += Time.deltaTime;
        imageCanvasGroup.alpha = m_Timer / fadeDuration;
        //creates a fade out to the game over screen

        if (m_Timer > fadeDuration + displayImageDuration)
        {
            if (doRestart)
            {
                SceneManager.LoadScene(0);
                //reloads the game if the player chooses to restart
            }
            else
            {
                Application.Quit();
                //quits the game
            }
        }
    }
}