﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI; //grants access to the NavMeshAgent class

public class WaypointPatrol : MonoBehaviour
{
    public NavMeshAgent navMeshAgent;
    //declares the agent as a public variable
    public Transform[] waypoints;
    //declares the waypoints variable

    int m_CurrentWaypointIndex;
    //allows the agent to know which waypoint it needs to head to next by keeping track of the order

    void Start()
    {
        navMeshAgent.SetDestination(waypoints[0].position);
    }

    void Update()
    {
        if (navMeshAgent.remainingDistance < navMeshAgent.stoppingDistance)  //checks to see if the agent is at its destination
        {
            m_CurrentWaypointIndex = (m_CurrentWaypointIndex + 1) % waypoints.Length;
            navMeshAgent.SetDestination(waypoints[m_CurrentWaypointIndex].position);
            //this is the initial set desitination of the agent
        }
    }
}
